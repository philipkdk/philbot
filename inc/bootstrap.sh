#!/bin/bash
#
# Installs all the stuff philbot needs
#
if [ ! -d /opt/philbot ];then
   cd /opt/
   git clone https://bitbucket.org/philipk/philbot
   chmod +x /opt/philbot/shell/philbot
   ln -s /opt/philbot/shell/philbot /usr/local/bin/philbot
fi

if [ -d /opt/philbot ];then
   cd /opt/philbot
   git pull
fi

# Europe/Copenhagen for timezone
if [ ! -f /etc/localtime.bak ]; then
  mv /etc/localtime /etc/localtime.bak
  ln -s /usr/share/zoneinfo/Europe/Copenhagen /etc/localtime
fi

# LAMP install
if [ ! -f /usr/sbin/httpd ]; then
  yum install -y epel-release
  yum install -y httpd httpd-itk mod_ssl
  yum install -y php5
  yum install -y php php-gd php-mysql php-tidy php-xmlrpc php-common php-cli php-xml
  yum install -y php-mcrypt php-soap php-mbstring
  yum -y install mariadb mariadb-server
  mysql -e "UPDATE mysql.user SET Password = PASSWORD('${SQLROOTPW}') WHERE User = 'root'"
  mysql -e "DROP USER ''@'localhost'"
  mysql -e "DROP USER ''@'$(hostname)'"
  mysql -e "DROP DATABASE test"
  mysql -e "FLUSH PRIVILEGES"
fi

if [ ! -f /usr/sbin/vsftpd ]; then
  yum install -y vsftpd
  touch /etc/vsftpd/chroot_list
  mkdir /etc/vsftpd/userconf/
  groupadd ftpaccess
  systemctl enable vsftpd.service
  systemctl restart vsftpd.service
fi

# Download WP-CLI
if [ ! -f /usr/local/bin/wp ]; then
  cd /tmp
  curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
  chmod +x wp-cli.phar
  mv wp-cli.phar /usr/local/bin/wp
fi

# Create LetsEncrypt webuser
if [ ! -d /var/www/letsencrypt/ ];then
	yum install -y python-certbot-apache
	echo "authenticator = webroot" > /etc/letsencrypt/cli.ini
	echo "renew-by-default" >> /etc/letsencrypt/cli.ini
	echo "agree-tos" >> /etc/letsencrypt/cli.ini
  useradd letsencrypt -d /var/www/letsencrypt -G apache -s /sbin/nologin
  chgrp -R apache /var/www/letsencrypt
  chmod -R g+w /var/www/letsencrypt
  chmod g+s /var/www/letsencrypt
  chmod 775 /var/www/letsencrypt
fi
