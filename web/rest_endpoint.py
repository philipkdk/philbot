#!/usr/bin/env python2
# -*- coding: utf-8 -*-
from flask import Flask
from subprocess import call

app = Flask(__name__)

@app.route('/')
def index():
    return "philbot rest api"

@app.route('/web/<user>/<domain>/', methods=['POST'])
def vhost_add(user, domain):
    if user and domain:
        if check_var(user) and check_var(domain):
            call(["/usr/local/bin/philbot", "web", "add", user, domain])
            return user + " and " + domain + " added!"
        else:
            return user + " or " + domain + " contains illegal characters!"
    else:
        return user + " and " + domain + " not set!"

@app.route('/web/<user>/<domain>/', methods=['DELETE'])
def vhost_del(user, domain):
    if user and domain:
        if check_var(user) and check_var(domain):
            call(["/usr/local/bin/philbot", "web", "del", user, domain])
            return user + " and " + domain + " deleted!"
        else:
            return user + " or " + domain + " contains illegal characters!"
    else:
        return user + " and " + domain + " not set!"

@app.route('/ssl/<user>/<domain>/', methods=['POST'])
def ssl_add(user, domain):
    if user and domain:
        if check_var(user) and check_var(domain):
            #call(["/usr/local/bin/philbot", "web", "ssl", user, domain])
            return user + " and " + domain + " got ssl certificate now!"
        else:
            return user + " or " + domain + " contains illegal characters!"
    else:
        return user + " and " + domain + " not set!"

@app.route('/sql/<user>/<domain>/', methods=['POST'])
def get_sql(user, domain):
    if user and domain:
        if check_var(user) and check_var(domain):
            call(["/usr/local/bin/philbot", "backup", "sql", user, domain])
            return "Database now available <a href='/dl/" + user +"_" + domain + ".sql'>here</a>!"
        else:
            return user + " or " + domain + " contains illegal characters!"
    else:
        return user + " and " + domain + " not set!"

@app.route('/www/<user>/<domain>/', methods=['POST'])
def get_www(user, domain):
    if user and domain:
        if check_var(user) and check_var(domain):
            call(["/usr/local/bin/philbot", "backup", "www", user, domain])
            return "www tar.gz now available <a href='/dl/" + user +"_" + domain + ".tar.gz'>here</a>!"
        else:
            return user + " or " + domain + " contains illegal characters!"
    else:
        return user + " and " + domain + " not set!"

@app.route('/log/<user>/<domain>/', methods=['POST'])
def get_log(user, domain):
    if user and domain:
        if check_var(user) and check_var(domain):
            call(["/usr/local/bin/philbot", "backup", "log", user, domain])
            return "log tar.gz now available <a href='/dl/" + user +"_" + domain + ".tar.gz'>here</a>!"
        else:
            return user + " or " + domain + " contains illegal characters!"
    else:
        return user + " and " + domain + " not set!"

def check_var(input):
    return not any(s in input for s in ('*', ','))

if __name__ == '__main__':
    app.run(debug=True)
