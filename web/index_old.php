<?php
// Philbot webinterface
?>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
<script src="js/bootstrap-show-password.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<style>
body {font-family: 'Oswald', sans-serif;}
th {color: #898989;}
td {color: #555555;}
</style>
</head>
<body
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/">admin@<?php $hostname = gethostname(); echo $hostname ?> ~$
      <span style="color:#555555;">..</span></a>
    </div>
  </div>
</nav>
<br>
<div class="container-fluid">
  <table id="users" class="table table-hover">
    <thead>
      <tr>
	    <th>DEL</th>
        <th>USERNAME</th>
        <th>PASSWORD</th>
        <th>DOMAIN</th>
        <th>APPICATION</th>
        <th>SSL</th>
        <th>BACKUP</th>
      </tr>
    </thead>
    <tfoot>
      <tr>
		<th>DEL</th>
		<th>USERNAME</th>
		<th>PASSWORD</th>
        <th>DOMAIN</th>
        <th>APPICATION</th>
        <th>SSL</th>
        <th>BACKUP</th>
      </tr>
    </tfoot>
    <tbody>
<?php
$handle = fopen("users.csv", "r");
for ($i = 0; $row = fgetcsv($handle ); ++$i) {
    echo "<tr>";
 /* echo "<td>[<a href='/del?u=" . $row[0] . "&d=" . $row[2] . "' class='delete'>X</a>]</td>";  */
    echo "<td><button id='" . $row[0] . "' class='btn btn-danger delete' value=" . $row[0] ." data-value=" . $row[2] . ">X</button></td>";
    echo "<td>" . $row[0] . "</td>";
    echo "<td><p><input data-toggle='password' data-placement='before' class='form-control' type='password' value='" . $row[1] ."' placeholder='password'></p></td>";
    echo "<td>" . $row[2] . "<span style='float:right'><a href='/dl?log=" . $row[2] . "'>log</a>/<a href='http://whois.orangenames.com/?action=lookup&d=" . $row[2] . "' target='_blank'>whois</a></span></td>";
    echo "<td>" . $row[3] . "</td>";
    echo "<td>" . $row[4] ."</td>";
    echo "<td><a href='/backup?t=sql&u=" . $row[0] . "&d=" . $row[2] . "' class='backup'>sql</a></td>";
    echo "</tr>";
}
fclose($handle);
?>
    </tbody>
  </table>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('#users').DataTable( {
        "order": [[ 1, "asc" ]],
        "pageLength": 20
    } );
} );
</script>
<script>
    $(function() {
        $('#password').password().on('show.bs.password', function(e) {
            $('#eventLog').text('On show event');
            $('#methods').prop('checked', true);
        }).on('hide.bs.password', function(e) {
                    $('#eventLog').text('On hide event');
                    $('#methods').prop('checked', false);
                });
        $('#methods').click(function() {
            $('#password').password('toggle');
        });
    });
</script>
<script>
    $(document).on("click", ".delete", function(e) {
    	var User = $(this).val();
        var Domain = $(this).attr("data-value");
        var newurl = '/web/' + User + '/' + Domain;
        bootbox.confirm("Are you sure you want to delete?", function(result) {
            if(result){
	            location.href = newurl;
            }
        });
    });
</script>
</html>
