$(document).ready(function() {
    $('#users').DataTable( {
        "order": [[ 1, "asc" ]],
        "pageLength": 20
    } );
} );

function pleaseWait() {
  var waitdialog = bootbox.dialog({
      title: 'Philbot',
      message: '<p><i class="fa fa-spin fa-spinner"></i> Please wait buddy..</p>',
      closeButton: false
  });
};

function pleaseHide() {
  $('.bootbox.modal.fade.in').modal('hide');
};

$(function() {
    $('#password').password().on('show.bs.password', function(e) {
        $('#eventLog').text('On show event');
        $('#methods').prop('checked', true);
    }).on('hide.bs.password', function(e) {
                $('#eventLog').text('On hide event');
                $('#methods').prop('checked', false);
            });
    $('#methods').click(function() {
        $('#password').password('toggle');
    });
});

$(document).on("click", ".vhost-del", function(e) {
	  var User = $(this).val();
    var Domain = $(this).attr("data-value");
    bootbox.confirm("Are you sure you want to delete?", function(result) {
        if(result){
          pleaseWait();
          jQuery.ajax({
          type: 'DELETE',
          url: "/api/web/" + User + "/" + Domain + "/",
          success: function(data) {
              pleaseHide();
              bootbox.dialog({
                  message: data,
                  title: "Philbot",
                  buttons: {
                      success: {
                          label: "Fedt man - Spa!",
                          className: "btn-success",
                      },
                  }
              });
          }
          });
        }
    });
});

$(document).on("click", ".sql", function(e) {
	  var User = $(this).val();
    var Domain = $(this).attr("data-value");
    bootbox.confirm("Are you sure you want to backup sql?", function(result) {
        if(result){
          pleaseWait();
          jQuery.ajax({
          type: 'POST',
          url: "/api/sql/" + User + "/" + Domain + "/",
          success: function(data) {
              pleaseHide();
              bootbox.dialog({
                  message: data,
                  title: "Philbot",
                  buttons: {
                      success: {
                          label: "Fedt man - Spa!",
                          className: "btn-success",
                      },
                  }
              });
          }
          });
        }
    });
});

$(document).on("click", ".www", function(e) {
	  var User = $(this).val();
    var Domain = $(this).attr("data-value");
    bootbox.confirm("Are you sure you want to backup www?", function(result) {
        if(result){
          pleaseWait();
          jQuery.ajax({
          type: 'POST',
          url: "/api/www/" + User + "/" + Domain + "/",
          success: function(data) {
              pleaseHide();
              bootbox.dialog({
                  message: data,
                  title: "Philbot",
                  buttons: {
                      success: {
                          label: "Fedt man - Spa!",
                          className: "btn-success",
                      },
                  }
              });
          }
          });
        }
    });
});

$(document).on("click", ".log", function(e) {
	  var User = $(this).val();
    var Domain = $(this).attr("data-value");
    bootbox.confirm("Are you sure you want to backup logs?", function(result) {
        if(result){
          pleaseWait();
          jQuery.ajax({
          type: 'POST',
          url: "/api/log/" + User + "/" + Domain + "/",
          success: function(data) {
              pleaseHide();
              bootbox.dialog({
                  message: data,
                  title: "Philbot",
                  buttons: {
                      success: {
                          label: "Fedt man - Spa!",
                          className: "btn-success",
                      },
                  }
              });
          }
          });
        }
    });
});

var User;
var Domain;

function setUser(type) {
  bootbox.prompt("Username", function(result) {
    User = result;
    setDomain(type);
  });
};

function setDomain(apipath) {
  bootbox.prompt("Domain", function(result) {
    Domain = result,
    callAPI(apipath);
  });
};

function callAPI(path, user, domain) {
    if(user && domain) {
      $.ajax({
      type: 'POST',
      url: "/api/"+ path +"/" + user + "/" + domain + "/",
      success: function(data) {
        swal({
          title: 'Spa!',
          text: 'Det er nu oprettet! 8D',
          type: 'success',
          timer: 5000,
        }, function() {
          window.location.reload();
        });
      },
      error: function() {
      	console.log("keine api dude!");
      }
      });
    };
};

function addToAPI(type) {
  swal({
      title: "oHai!",
      text: "Please type in a username!",
      type: "input",
      showCancelButton: true,
      closeOnConfirm: false,
      animation: "slide-from-top",
      inputPlaceholder: "Username"
    },
    function(username){
      if (username === false) return false;

      if (username === "") {
        swal.showInputError("You need to write something!");
        return false
      }

      swal({
          title: "oHai again!",
          text: "Please type in a domain name!",
          type: "input",
          showCancelButton: true,
          closeOnConfirm: false,
          animation: "slide-from-top",
          inputPlaceholder: "Domain"
        },
        function(domain){
          if (domain === false) return false;

          if (domain === "") {
            swal.showInputError("You need to write something!");
            return false
          }

          callAPI(type, username, domain);
        });
    });
};
