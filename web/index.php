<html>
<head>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<link href="css/philbot.css" rel="stylesheet">
<script src="js/bootstrap-show-password.js"></script>
<script src="js/philbot.js"></script>
</head>
</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <ul class="nav navbar-nav navbar-right">
        <li class="active"><a href="#">/<span class="sr-only">(current)</span></a></li>
        <li><a href="#" id="vhost-add">ADD VHOST</a></li>
        <li><a href="#" id="ssl-add">ADD SSL</a></li>
      </ul>
      <a class="navbar-brand" href="/">admin@<?php $hostname = gethostname(); echo $hostname ?> ~$
      <span style="color:#555555;">..</span></a>
    </div>
  </div>
</nav>
<hr style="margin-bottom:-20px;">
<script>
$('#vhost-add').click(function() {
    addToAPI("web");
});
$('#ssl-add').click(function() {
    addToAPI("ssl");
});
</script>
<div class="jumbotron">
  <div class="container-fluid">
    <table id="users" class="table table-hover">
      <thead>
        <tr>
          <th>DEL</th>
          <th>USERNAME</th>
          <th>PASSWORD</th>
          <th>DOMAIN</th>
          <th>APPICATION</th>
          <th>SSL</th>
          <th>SQL USER</th>
          <th>SQL PASS</th>
          <th>SQL DB</th>
          <th>BACKUP</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th>DEL</th>
          <th>USERNAME</th>
          <th>PASSWORD</th>
          <th>DOMAIN</th>
          <th>APPICATION</th>
          <th>SSL</th>
          <th>SQL USER</th>
          <th>SQL PASS</th>
          <th>SQL DB</th>
          <th>BACKUP</th>
        </tr>
      </tfoot>
      <tbody>
        <?php include 'inc/users.php'; ?>
      </tbody>
    </table>
  </div>
</div>
<footer class="footer">
  <div class="container-fluid">
    <p class="text-muted">Philbot - Ready to handle your tasks, buddy!</p>
  </div>
</footer>
</body>
</html>
